
# Liteos-a编译系统分析

## 1.1 怎么编译子目录

以kernel/liteos_a/fs/fat/Makefile为例：

![kernel/liteos_a/fs/fat/Makefile](pic\04_Liteos-a的编译系统\004_fat_makefile.png)

* 第1行包含config.mk
  
   这是包含一些预先定义的变量，比如默认的编译选项等。
   
* 下面的代码定义了LOCAL_SRCS
  
   等于一系列C文件，这就是要编译的源文件。
   
* 定义了LOCAL_INCLUDE

   这是头文件的目录
   
* 定义了LOCAL_FLAGS

   这是编译选项

* 定义了MODULE_NAME
  
  一般等于当前目录的名字，比如fat，以后就编译得到libfat.a
  
* 怎么编译？看最后一行

   ```
   include $(MODULE)
   ```

   MODULE就是：

   ```
   MODULE = $(MK_PATH)/module.mk  # kernel/liteos_a/tools/build/mk/module.mk
   ```

   分析module.mk：

   ```
   # 找到第1个目标
   all : $(LIB)
   
   # LIB是什么, 如果没定义LOCAL_SO，LIB就是 lib$(MODULE_NAME).a, 比如 libfat.a
   ifeq ($(LOCAL_SO), y)
   LIBSO := $(OUT)/lib/lib$(MODULE_NAME).so
   LIBA := $(OUT)/lib/lib$(MODULE_NAME).a
   else
   LIBSO :=
   LIBA := $(OUT)/lib/lib$(MODULE_NAME).a
   endif
   LIB := $(LIBA) $(LIBSO)
   
   # 怎么编译 LIBA ? 看下图
   ```


![module](pic\04_Liteos-a的编译系统\005_module.png)



## 1.2 编译哪些子目录

### 1.2.1 从链接命令看内核的组成

* 链接命令如下

  liteos-a由一系列的库文件组成，reset_vector是它的入口。

![link_liteos-a](pic\04_Liteos-a的编译系统\001_liteos-a_link.png)



* 查看链接脚本

  ![liteos_llvm.ld](pic\04_Liteos-a的编译系统\002_liteos_llvm.ld.png)

* 查看liteos.map文件

  <img src="pic\04_Liteos-a的编译系统\003_liteos_map.png" alt="liteos.map" style="zoom:150%;" />



### 1.2.2 从Makefile开始分析

从链接命令查看内核的组成，是一个取巧的方法。
本质的方法应该是从kernel/liteos_a/Makefile开始分析。



## 1.3 顶层Makefile分析

指的是kernel/liteos_a/Makefile。

### 1.3.1 Makfile中常用变量

1. LITEOSTOPDIR                        // kernel/liteos_a
2. LITEOSTHIRDPARTY               // third_party
3. LITEOS_MK_PATH                   // kernel/liteos_a/tools/build/mk
4. MK_PATH  = $(LITEOSTOPDIR)/tools/build/mk       // kernel/liteos_a/tools/build/mk

### 1.3.2 包含的文件

Makefile里使用include命令包含了很多文件，这些文件又包含了其他文件。

#### 1. STM32MP157

```shell
Makefile
	-include $(LITEOSTOPDIR)/tools/build/config.mk
		-include $(LITEOSTOPDIR)/tools/build/mk/los_config.mk
			-include $(LITEOSTOPDIR)/.config
			include $(LITEOSTOPDIR)/arch/cpu.mk
				-include $(LITEOSTOPDIR)/arch/arm/arm.mk
			include $(LITEOSTOPDIR)/platform/bsp.mk
			include $(LITEOSTOPDIR)/../../vendor/st/stm32mp157/stm32mp157.mk
			include $(LITEOSTOPDIR)/../../drivers/hdf/lite/hdf_lite.mk
				include $(LITEOSTOPDIR)/../../vendor/st/hdf/hdf_vendor.mk
			-include $(LITEOSTOPDIR)/3rdParty/3rdParty.mk
		-include $(LITEOS_MK_PATH)/liteos_tables_ldflags.mk
	-include $(LITEOS_MK_PATH)/dynload.mk
```
#### 2. IMX6ULL

```shell
Makefile
	-include $(LITEOSTOPDIR)/tools/build/config.mk
		-include $(LITEOSTOPDIR)/tools/build/mk/los_config.mk
			-include $(LITEOSTOPDIR)/.config
			include $(LITEOSTOPDIR)/arch/cpu.mk
				-include $(LITEOSTOPDIR)/arch/arm/arm.mk
			include $(LITEOSTOPDIR)/platform/bsp.mk
			include $(LITEOSTOPDIR)/../../vendor/nxp/imx6ull/imx6ull.mk
			include $(LITEOSTOPDIR)/../../drivers/hdf/lite/hdf_lite.mk
				include $(LITEOSTOPDIR)/../../vendor/nxp/hdf/hdf_vendor.mk
			-include $(LITEOSTOPDIR)/3rdParty/3rdParty.mk
		-include $(LITEOS_MK_PATH)/liteos_tables_ldflags.mk
	-include $(LITEOS_MK_PATH)/dynload.mk
```

#### 3. 展开Makefile

把Makefile里被包含的文件都展开，这样便于分析。
我们事先对STM32MP157、IMX6ULL展开了它们的Makefile，结果分别保存在这两个文件里：

```
Makefile_all_stm32mp157.txt
Makefile_all_imx6ull.txt
```

**注意**：`-include`表示有文件就包含，没有文件就不包含。

### 1.3.3 分析make过程

#### 1. 第1个目标
all: $(OUT) $(BUILD) $(LITEOS_TARGET) $(APPS)

 #### 2. 目标: OUT

* OUT目标：

  创建了目录：kernel/liteos_a/imx6ull/lib

  把board.ld.S编译成了board.ld，这是链接文件。
```
# .config文件中， LOSCFG_PLATFORM="imx6ull"
OUT  = $(LITEOSTOPDIR)/out/$(LITEOS_PLATFORM)

$(OUT): $(LITEOS_MENUCONFIG_H)
	$(HIDE)mkdir -p $(OUT)/lib
	$(HIDE)$(CC) -I$(LITEOS_PLATFORM_BASE)/include -I$(BOARD_INCLUDE_DIR) \
		-E $(LITEOS_PLATFORM_BASE)/board.ld.S \
		-o $(LITEOS_PLATFORM_BASE)/board.ld -P
```
* OUT的依赖：LITEOS_MENUCONFIG_H

  配置内核，生成头文件autoconf.h。
```
LITEOS_MENUCONFIG_H = $(LITEOSTOPDIR)/include/generated/autoconf.h

KCONFIG_FILE_PATH = $(LITEOSTOPDIR)/Kconfig

$(LITEOS_MENUCONFIG_H):
ifneq ($(LITEOS_PLATFORM_MENUCONFIG_H), $(wildcard $(LITEOS_PLATFORM_MENUCONFIG_H)))
	$(HIDE)$(MAKE) genconfig
endif

genconfig:$(MENUCONFIG_PATH)/conf
	$(HIDE)mkdir -p include/config include/generated
	$< --silentoldconfig $(KCONFIG_FILE_PATH)
	-mv -f $(LITEOS_MENUCONFIG_H) $(LITEOS_PLATFORM_MENUCONFIG_H)
```
#### 3. 目标: BUILD
创建目录 `kernel/liteos_a/out/stm32mp157/obj`或`kernel/liteos_a/out/imx6ull/obj`

```
OUT  = $(LITEOSTOPDIR)/out/$(LITEOS_PLATFORM)
BUILD  = $(OUT)/obj
$(BUILD):
	$(HIDE)mkdir -p $(BUILD)
```

#### 4. 目标: LITEOS_TARGET

这是核心，进入子目录执行make，把子目录中的文件链接为一个库。

最后，把这些库链接为liteos内核。

```
LITEOS_TARGET = liteos
$(LITEOS_TARGET): $(__LIBS)
	$(HIDE)touch $(LOSCFG_ENTRY_SRC)

	$(HIDE)for dir in $(LITEOS_SUBDIRS); \
	do $(MAKE) -C $$dir all || exit 1; \
	done

	$(LD) $(LITEOS_LDFLAGS) $(LITEOS_TABLES_LDFLAGS) $(LITEOS_DYNLDFLAGS) -Map=$(OUT)/$@.map -o $(OUT)/$@ --start-group $(LITEOS_LIBDEP) --end-group
#	$(SIZE) -t --common $(OUT)/lib/*.a >$(OUT)/$@.objsize
	$(OBJCOPY) -O binary $(OUT)/$@ $(LITEOS_TARGET_DIR)/$@.bin
	$(OBJDUMP) -t $(OUT)/$@ |sort >$(OUT)/$@.sym.sorted
	$(OBJDUMP) -d $(OUT)/$@ >$(OUT)/$@.asm
#	$(NM) -S --size-sort $(OUT)/$@ >$(OUT)/$@.size
```
* 目标：__LIBS

  ```
  # 没做什么事
  __LIBS = libs
  $(__LIBS): $(OUT) $(CXX_INCLUDE)
  ```

  

* 命令：$(HIDE)touch $(LOSCFG_ENTRY_SRC)

  每次都要编译los_config.c，touch一下

  ```
  LOSCFG_ENTRY_SRC    = $(LITEOSTOPDIR)/kernel/common/los_config.c
  ```

* 命令：进入每个LITEOS_SUBDIRS，执行make

  ```
  # LIB_SUBDIRS 等于一系列的目录
  LIB_SUBDIRS :=
  LIB_SUBDIRS             += arch/arm/$(LITEOS_ARCH_ARM)  # 就是arch/arm/arm
  LIB_SUBDIRS             += $(PLATFORM_BSP_HISI_BASE)
  LIB_SUBDIRS     += $(LITEOSTOPDIR)/kernel/common
  LIB_SUBDIRS       += kernel/base
  LIB_SUBDIRS     += $(IMX6ULL_BASE_DIR)/board
  LIB_SUBDIRS     += $(IMX6ULL_BASE_DIR)/driver/mtd/common
  LIB_SUBDIRS     += $(IMX6ULL_BASE_DIR)/driver/mtd/spi_nor
  LIB_SUBDIRS             += $(IMX6ULL_BASE_DIR)/driver/imx6ull-fb
  LIB_SUBDIRS             += $(IMX6ULL_BASE_DIR)/driver/imx6ull-uart
  LIB_SUBDIRS         += kernel/extended/cpup
  LIB_SUBDIRS        += lib/libc
  LIB_SUBDIRS        += lib/libsec
  LIB_SUBDIRS         += lib/libscrew
  LIB_SUBDIRS     += fs/fat
  LIB_SUBDIRS     += fs/jffs2
  
  LITEOS_SUBDIRS   = $(LIB_SUBDIRS)
  
  $(HIDE)for dir in $(LITEOS_SUBDIRS); \
  	do $(MAKE) -C $$dir all || exit 1; \
  	done
  ```
* 链接及各类处理
```
	$(LD) $(LITEOS_LDFLAGS) $(LITEOS_TABLES_LDFLAGS) $(LITEOS_DYNLDFLAGS) -Map=$(OUT)/$@.map -o $(OUT)/$@ --start-group $(LITEOS_LIBDEP) --end-group
#	$(SIZE) -t --common $(OUT)/lib/*.a >$(OUT)/$@.objsize
	$(OBJCOPY) -O binary $(OUT)/$@ $(LITEOS_TARGET_DIR)/$@.bin
	$(OBJDUMP) -t $(OUT)/$@ |sort >$(OUT)/$@.sym.sorted
	$(OBJDUMP) -d $(OUT)/$@ >$(OUT)/$@.asm
#	$(NM) -S --size-sort $(OUT)/$@ >$(OUT)/$@.size
```

## 1.4 核心总结

### 1.4.1 生成内核的过程

* 配置内核时，配置信息放在.config里，比如：`LOSCFG_FS_FAT=y`

* kernel/liteos_a/Makefile会包含很多文件，比如`kernel/liteos_a/tools/build/mk/los_config.mk`

  * 它会看到`LIB_SUBDIRS`和`LITEOS_BASELIB`这两个变量

  * `LIB_SUBDIRS`表示要进入那个子目录编译

  * `LITEOS_BASELIB`表示要把哪些库链接进内核

  * 比如：

    ```shell
    ifeq ($(LOSCFG_FS_FAT), y)
        LITEOS_BASELIB  += -lfat
        LIB_SUBDIRS     += fs/fat
        LITEOS_FAT_INCLUDE += -I $(LITEOSTHIRDPARTY)/FatFs/source
    endif
    ```

* 编译子目录：进入`LIB_SUBDIRS`指定的子目录，执行`make all`

  * 对于子目录xxx的编译，一般是生成libxxx.a

* 链接内核
  * 把` LITEOS_BASELIB`指定的库都链接进内核

### 1.4.2 增加模块

* 在kernel/liteos_a/Makefile能包含的文件中，增加这2个变量：
  * LIB_SUBDIRS     += 源码目录/xxx
  *  LITEOS_BASELIB  += -lxxx

* 在`源码目录/xxx`下仿照`fs/fat/Makefile`增加Makefile