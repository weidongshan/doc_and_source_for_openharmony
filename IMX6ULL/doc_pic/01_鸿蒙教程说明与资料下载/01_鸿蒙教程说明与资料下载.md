# 1. 鸿蒙是什么

## 1.1 鸿蒙体系

![](pic\01_introduction _and_download\001_harmony_framework.png)

## 1.2 仿照windows来理解鸿蒙

![](pic\01_introduction _and_download\002_embeded_soft_parts.png)

# 2. 鸿蒙的开发环境

## 2.1 单片机的开发环境

![](pic\01_introduction _and_download\003_kei_ide.png)

## 2.2 鸿蒙的IDE

鸿蒙的IDE分为南向IDE、北向IDE。上北下南，北向只APP，南向指内核。
南向IDE的下载地址：https://device.harmonyos.com/cn/ide
它的功能将会很强大，但是目前状况为：

* 可以在Windows/Linux下阅读源码

* 但是编译的话，只能在Linux下使用命令行来编译

* 可以使用Jlink来调试，目前只支持在Windows下调试

这个IDE已经支持IMX6ULL的调试了，后面需要的话我会介绍调试功能。

## 2.3 传统的开发方式

### 2.3.1 有专门的Linux服务器

![](pic\01_introduction _and_download\004_linux_server_for_harmony.png)

### 2.3.2 使用VMWare做Linux服务器

![](pic\01_introduction _and_download\005_vmware_for_harmony.png)



# 3. 本教程会讲什么内容

## 3.1 最小系统移植

所谓最小系统，就是在开发板上能运行鸿蒙内核、进入鸿蒙根文件系统、可以执行命令行程序，可以运行hello程序。
我们已经在百问网的IMX6ULL、STM32MP157开发板上成功移植了鸿蒙最小系统。



## 3.2 鸿蒙驱动开发

将会涉及这些驱动：LCD、触摸屏、I2C、SPI、GPIO、EMMC、网卡等。
驱动开发课程永远不会完结，如果有新的模块录制需求，我们就会去录制。



# 4. 资料下载

本教程资料放在GIT上：

* https://e.coding.net/weidongshan/openharmony/doc_and_source_for_openharmony.git

为方便记忆，你也可以访问百问网：

* 视频：http://www.100ask.net
* 下载中心：http://download.100ask.net/



# 5. 强烈建议

先去https://gitforwindows.org/下载Windows版本的git工具。

安装、启动Git Bash。

然后执行命令：
```
git clone  https://e.coding.net/weidongshan/openharmony/doc_and_source_for_openharmony.git`
```