# GenericTimer源码分析

## 1.1 GenericTimer使用方法

```mermaid
graph LR
clk_source[设置时钟源]
SystemCounter[设置/启动SystemCounter]
timer0["设置Processor0的Timer: 设置比较值、使能中断、使能Timer"]
timer1["设置Processor1的Timer: 设置比较值、使能中断、使能Timer"]
clk_source --> SystemCounter --> timer0
SystemCounter --> timer1
```

* 设置时钟源：芯片相关，一般u-boot里做好了
* 设置/启动SystemCounter：一般u-boot里做好了
* 设置Processor的Timer：
  * 设置比较值、使能中断、使能Timer
  * 注册中断处理函数



## 1.2 源码分析

代码：`kernel\liteos_a\platform\hw\arm\timer\arm_generic\arm_generic_timer.c`

### 1.2.1 初始化

它做了2件事：

* 读出SystemCounter的频率：以后设置中断周期时要用
* 注册中断处理函数

```c
LITE_OS_SEC_TEXT_INIT VOID HalClockInit(VOID)
{
    UINT32 ret;

    g_sysClock = HalClockFreqRead();
    ret = LOS_HwiCreate(OS_TICK_INT_NUM, MIN_INTERRUPT_PRIORITY, 0, OsTickEntry, 0);
    if (ret != LOS_OK) {
        PRINT_ERR("%s, %d create tick irq failed, ret:0x%x\n", __FUNCTION__, __LINE__, ret);
    }
}
```

### 1.2.2 启动Timer

它做了2件事：

* 使能中断：中断号是29
* 设置TimerValue寄存器：
  * OS_CYCLE_PER_TICK  = g_sysClock / 100，也就是10MS之后产生中断
  * 设置TimerValue寄存器的实质，就是设置**比较寄存器(CVAL) = 当前SystemCounter值 + OS_CYCLE_PER_TICK  **

```c
LITE_OS_SEC_TEXT_INIT VOID HalClockStart(VOID)
{
    HalIrqUnmask(OS_TICK_INT_NUM);

    /* triggle the first tick */
    TimerCtlWrite(0);
    TimerTvalWrite(OS_CYCLE_PER_TICK);
    TimerCtlWrite(1);
}
```

### 1.2.3 中断处理

它做了2件事：

* 调用OsTickHandler
* 设置下一次中断时间：
  * 设置**比较寄存器(CVAL) = 当前比较寄存器值 + OS_CYCLE_PER_TICK  **
  * 为什么不是**当前SystemCounter值 + OS_CYCLE_PER_TICK**？
  * 因为处理中断也是要时间的，SystemCounter值一直在增加
  * 要让两次中断的间隔非常精确的话，要使用发生中断时的SystemCounter值，也就是比较寄存器的当前值

```c
LITE_OS_SEC_TEXT VOID OsTickEntry(VOID)
{
    TimerCtlWrite(0);

    OsTickHandler();

    /*
     * use last cval to generate the next tick's timing is
     * absolute and accurate. DO NOT use tval to drive the
     * generic time in which case tick will be slower.
     */
    TimerCvalWrite(TimerCvalRead() + OS_CYCLE_PER_TICK);
    TimerCtlWrite(1);
}
```

