# Liteos-a中串口的使用

## 1.1 内核里打印

内核打印函数是PRINT_RELEASE，它的内部调用关系如下：

```c
PRINT_RELEASE
    LOS_LkPrint
    	g_osLkHook
    		OsLkDefaultFunc
    			OsVprintf
    				UartPuts
    					UartPutsReg
    						UartPutStr
    							UartPutcReg
```

我们要实现`UartPutcReg`，用来输出单个字符。



## 1.2 APP控制台

我们编写的应用程序，调用printf时，那些信息从哪里打印出来？从**控制台**。
在串口上运行程序，**控制台**就是串口。
远程登录板子后运行程序，**控制台**就是远程登录终端。

控制台的实现分为4层：
```mermaid
graph TD
user_dev["/dev/console1"]
serial_dev["/dev/serial"]
telnet_dev["/dev/telnet"]
uart_dev0["/dev/uartdev-0"]
uart_dev1["/dev/uartdev-1"]
uart_ops["uart_ops"]
user_dev --> serial_dev
user_dev --> telnet_dev
serial_dev --> uart_dev0
serial_dev --> uart_dev1
uart_dev0 --> uart_ops
```

### 1.2.1 /dev/console

init进程打开的就是/dev/console，它会打开shell。
我们在shell里执行各种APP时，这些APP会继承父进程的3个设备：标准输入、标准输出、标准错误，都对应`/dev/console`。
我们编写的APP，一般不需要自己去打开`/dev/console`，它已经继承得到了。
在串口上运行程序，`/dev/console`就是串口。
远程登录板子后运行程序，`/dev/console`就是远程登录终端。
所以`/dev/console`表示的是`当前终端`，它可能对应不同的设备，比如`/dev/serial`或`/dev/telnet`。



### 1.2.2 /dev/serial

在Liteos-a中，`/dev/serial`被称为**virtual serial**，虚拟串口。它只是起一个中转的作用，无论是APP还是内核，使用`/dev/serial`时，都是再次跳转去执行具体串口设备驱动程序的函数。比如：
![image-20201129224821832](pic/07_串口移植/01_liteos-a_virtual_serial_open.png)



那么，`/dev/serial`这个虚拟串口，怎么跟具体串口挂钩？也就是上图中，`GetFileOps`函数为何能得到具体串口的驱动程序？
方法如下图所示：
![image-20201129225337475](pic/07_串口移植/02_virtual_serial_init.png)

`virtual_serial_init`函数会找到`/dev/uartdev-0`的驱动程序(即它对应的`struct inode`，里面含有`file_operations_vfs`)。



### 1.2.3 /dev/uartddev-0

#### 1. 总体介绍

这是真正操作硬件的驱动程序，它分为两部分：`device_t`、`driver_t`。

* 在`device_t`中设置**资源**，比如寄存器物理基地址、中断号等
* 在`driver_t`中提供函数，比如`device_probe`、`device_attach`函数
  * 当内核发现有名字系统的`device_t`、`driver_t`时
  * 就会调用`driver_t`中的`device_probe`、`device_attach`函数
  * 在里面根据`device_t`得到**资源**、注册驱动`register_driver`

这种写驱动程序的方法，被称为**分离**：操作函数、资源分离。
以后想换一个硬件，只需要修改`device_t`就可以，`driver_t`保存不变。

#### 2. device_t

示例代码：

![image-20201201141317977](pic/07_串口移植/003_add_uart_device.png)

#### 3. drvier_t

先注册一个drvier_t结构体，它里面带有各类`device_method_t`：

![image-20201201142035130](pic/07_串口移植/004_add_uart_driver.png)

当内核发现有同名的`device_t`和`driver_t`时，就会调用`driver_t`里面提供的`device_probe`、`device_attach`函数。

#### 4. uartdev_fops

在`device_attach`函数里从`device_t`里获取硬件资源、注册驱动：
![image-20201201142545660](pic/07_串口移植/005_uart_attach.png)

`/dev/uartdev-0`对应的驱动程序时`uartdev_fops`，它通过`uart_ops`来操作硬件。

### 1.2.4 uart_ops

在UART驱动程序里，我们只需要提供硬件操作部分：
![image-20201201142839372](pic/07_串口移植/006_uart_ops.png)

uart_ops里有4个函数：

* config：配置串口，比如波特率等
* startup：启动串口，比如注册中断处理函数、启动串口
* start_tx：发送字符串
* shutdown：关闭串口

串口就两大功能：发送数据、接收数据。
在Liteos-a中，发送数据比较简单：没有使用中断，而是使用查询方式逐个发送，核心是`UartPutcReg`。
接收数据时使用中断，所以需要注册**串口接收中断处理函数**，它要做的事情是：

* 发生中断时，读取硬件获得字符，可能有多个字符
* 处理特殊字符：比如不\`\r\`换为\`\n\`
* 通知上层代码：`udd->recv(udd, buf, count);`