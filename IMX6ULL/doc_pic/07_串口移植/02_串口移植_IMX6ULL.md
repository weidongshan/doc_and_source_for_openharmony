# 串口移植

我们的目标是：让最小系统启动。
那么对于串口，不需要考虑得很全面：

* 不需要初始化串口：u-boot已经初始化串口了
* 不需要动态配置串口：固定使用某个波特率等配置就可以(在u-boot里设置过了)

移植工作只需要实现这几点：

* 串口发送单个字符

* 注册串口接收中断函数：确定中断号、使能中断、在中断函数中读取数据


## 1.1 最终结果

本章节做的修改会制作为补丁文件：

* `03_openharmony_uart_imx6ull.patch`

假设目录`openharmony`中是未修改的代码，从没打过补丁；
假设补丁文件放在openharmony的同级目录；
打补丁方法如下：

```
$ cd openharmony
$ patch -p1 < ../openharmony_100ask_v1.2.patch
$ patch -p1 < ../01_openharmony_add_demo_board.patch
$ patch -p1 < ../02_openharmony_memmap_imx6ull.patch 
$ patch -p1 < ../03_openharmony_uart_imx6ull.patch
```

打上补丁后，可以如此编译：

```
$ cd kernel/liteos_a
$ cp tools/build/config/debug/demochip_clang.config .config
$ make clean
$ make
```



## 1.2 串口发送单个字符



## 1.3 在device_t中指定资源

需要确定2个资源：寄存器地址、中断号



## 1.4 实现uart_ops

在UART驱动程序里，`uart_ops`结构体封装了UART的硬件操作：
![image-20201201142839372](pic/07_串口移植/006_uart_ops.png)

uart_ops里有4个函数：

* config：配置串口，比如波特率等
* startup：启动串口，比如注册中断处理函数、启动串口
* start_tx：发送字符串
* shutdown：关闭串口

我们只需要实现startup、start_tx，其他函数可以设为空：

* startup：确定中断号、request_irq、使能中断、提供中断处理函数
* start_tx：发送字符串



## 1.5 GIC

在`kernel\liteos_a\platform\main.c`中，调用`OsSystemInfo`打印系统信息时，代码如下：

```c
    PRINT_RELEASE("\n******************Welcome******************\n\n"
            "Processor   : %s"
#if (LOSCFG_KERNEL_SMP == YES)
            " * %d\n"
            "Run Mode    : SMP\n"
#else
            "\n"
            "Run Mode    : UP\n"
#endif
            "GIC Rev     : %s\n"
            "build time  : %s %s\n"
            "Kernel      : %s %d.%d.%d.%d/%s\n"
            "\n*******************************************\n",
            LOS_CpuInfo(),
#if (LOSCFG_KERNEL_SMP == YES)
            LOSCFG_KERNEL_SMP_CORE_NUM,
#endif
            HalIrqVersion(), __DATE__, __TIME__,\
            KERNEL_NAME, KERNEL_MAJOR, KERNEL_MINOR, KERNEL_PATCH, KERNEL_ITRE, buildType);
```



里面的`HalIrqVersion`函数用到的GIC的虚拟地址，要正确设置，否则没有打印信息。
IMX6ULL的内存映射代码里，设备空间从GIC开始映射，所以GIC的虚拟地址就是`PERIPH_DEVICE_BASE`：

```c
// kernel/liteos_a/kernel/base/include/los_vm_zone.h
#define GIC_VIRT_BASE    PERIPH_DEVICE_BASE

// vendor/democom/demochip/board/include/asm/platform.h
#define GIC_BASE_ADDR             (GIC_VIRT_BASE)
```

