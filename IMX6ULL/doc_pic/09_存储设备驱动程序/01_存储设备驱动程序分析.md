# 存储设备驱动程序分析

参考资料：`vendor\democom\demochip\driver\mtd\spi_nor\src\common\spinor.c`



## 1.1 字符设备和块设备

Linux中设备驱动程序分为3类：字符设备、块设备、网络设备。
所谓字符设备就是LED、按键、LCD、触摸屏这些非存储设备，APP可以直接调用驱动函数去操作它们。
而块设备就是Flash、磁盘这些存储设备，APP读写普通的文件时，最终会由驱动程序访问硬件。
为什么叫**块设备**？以前的磁盘读写时，是以块为单位的：即使只是读写一个字节，也需要读写一个块。
主要差别在于：

* 字符设备驱动程序里，可以读写任意长度的数据
* 块设备驱动程序里，读写数据时以块(扇区)为单位

### 1.1.1 APP与驱动程序的交互

#### 1. 字符设备驱动程序

![image-20201204170335133](pic/09_存储设备驱动程序/01_chrdev_driver.png)



#### 2. 块设备驱动程序教

![image-20201204170937651](pic/09_存储设备驱动程序/02_blockdev_driver.png)



### 1.1.2 驱动程序结构体

从上面的图形可以看到，无论是字符设备还是块设备，都要提供open/read/write/ioctl这些函数。
它们的驱动程序核心是类似的：

* 字符设备驱动程序：file_operations_vfs
  ![image-20201204171250346](pic/09_存储设备驱动程序/03_file_operations_vfs.png)

  

* 块设备驱动程序：block_operations
  ![image-20201204171355865](pic/09_存储设备驱动程序/04_block_operations.png)

### 1.1.3 注册函数

#### 1. 字符设备驱动程序注册函数

```c
int register_driver(FAR const char *path, FAR const struct file_operations_vfs *fops,
                    mode_t mode, FAR void *priv);
```

示例：

```c
int ret = register_driver("/dev/hello", &g_helloDevOps, 0666, NULL);
```



#### 2. 块设备驱动程序注册函数

```c
int register_blockdriver(FAR const char *path,
                         FAR const struct block_operations *bops,
                         mode_t mode, FAR void *priv);
```

示例：

```c
int ret = register_blockdriver("/dev/spinor", &g_dev_spinor_ops, 0755, mtd);
```



## 1.2 MTD设备

在各类电子产品中，存储设备类型多种多样，比如Nor Flash、Nand Flash，这些Flash又有不同的接口：比如SPI接口等等。
这些不同Flash的访问方法各有不同，但是肯定有这三种操作：

* 读
* 写
* 擦除

那么可以抽象出一个软件层：MTD，含义为**Memory Technology Device**，它封装了不同Flash的操作。主要是抽象出一个结构体：

```c
struct MtdDev {
    VOID *priv;
    UINT32 type;

    UINT64 size;
    UINT32 eraseSize;

    int (*erase)(struct MtdDev *mtd, UINT64 start, UINT64 len, UINT64 *failAddr);
    int (*read)(struct MtdDev *mtd, UINT64 start, UINT64 len, const char *buf);
    int (*write)(struct MtdDev *mtd, UINT64 start, UINT64 len, const char *buf);
};
```

不同的Flash要提供它自己的MtdDev结构体。



## 1.3 块设备驱动程序为MTD开了一个后门

视频里讲解。
![image-20201205223925387](pic/09_存储设备驱动程序/05_register_blockdriver_with_mtd.png)

在JFFS2文件系统中，是直接使用MTD的，没有使用block_operations。
比如：`third_party\Linux_Kernel\fs\jffs2\read.c`：

![image-20201205224617758](pic/09_存储设备驱动程序/06_jffs2_read_with_mtd.png)



## 1.4 怎么用内存模拟Flash

### 1.4.1 指定要使用的内存地址、大小

源码：`vendor\democom\demochip\driver\mtd\spi_nor\src\common\spinor.c`

![image-20201205224858302](pic/09_存储设备驱动程序/07_ramdisk_addr_size.png)

### 1.4.2 实现MtdDev结构体

源码：`vendor\democom\demochip\driver\mtd\spi_nor\src\common\spinor.c`

![image-20201205225026271](pic/09_存储设备驱动程序/08_ramdisk_ops.png)



### 1.4.3 怎么使用块设备

* 添加分区
  * /dev/spinor表示整个块设备
  * /dev/spinorblk0表示里面的第0个分区
  * 不添加分区也可以，可以直接挂载/dev/spinor
* mount

![image-20201205225759183](pic/09_存储设备驱动程序/09_use_mtd.png)