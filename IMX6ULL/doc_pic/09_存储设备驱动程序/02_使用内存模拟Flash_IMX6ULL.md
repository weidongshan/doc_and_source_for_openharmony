# 使用内存模拟Flash

参考资料：`vendor\democom\demochip\driver\mtd\spi_nor\src\common\spinor.c`



## 1.1 最终结果

本章节做的修改会制作为补丁文件：

* `04_openharmony_ramfs_imx6ull.patch`  

假设目录`openharmony`中是未修改的代码，从没打过补丁；
假设补丁文件放在openharmony的同级目录；
打补丁方法如下：

```
$ cd openharmony
$ patch -p1 < ../openharmony_100ask_v1.2.patch
$ patch -p1 < ../01_openharmony_add_demo_board.patch
$ patch -p1 < ../02_openharmony_memmap_imx6ull.patch 
$ patch -p1 < ../03_openharmony_uart_imx6ull.patch
$ patch -p1 < ../04_openharmony_ramfs_imx6ull.patch
```

打上补丁后，可以如此编译：

```
$ cd kernel/liteos_a
$ cp tools/build/config/debug/demochip_clang.config .config
$ make clean
$ make
```







## 1.2 原来的内存映射

![image-20201205232136210](pic/09_存储设备驱动程序/10_mem_map_origin.png)



## 1.3 在内存里挖出一块用来模拟Flash

![image-20201206092939573](pic/09_存储设备驱动程序/11_ramfs_mem_map.png)