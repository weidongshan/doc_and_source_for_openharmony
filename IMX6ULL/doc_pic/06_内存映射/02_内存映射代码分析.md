## 内存映射代码分析

分析启动文件`kernel\liteos_a\arch\arm\arm\src\startup\reset_vector_up.S`，
可以得到下图所示的地址映射关系：

* 内存地址
  * KERNEL_VMM_BASE开始的这块虚拟地址，使用Cache，速度快
  * UNCACHED_VMM_BASE开始的这块虚拟地址，不使用Cache，适合DAM传输、LCD Framebuffer等
* 设备空间：就是各种外设，比如UART、LCD控制器、I2C控制器、中断控制器
  * PERIPH_DEVICE_BASE开始的这块虚拟地址，不使用Cache不使用Buffer
  * PERIPH_CACHED_BASE开始的这块虚拟地址，使用Cache使用Buffer
  * PERIPH_UNCACHE_BASE开始的这块虚拟地址，不使用Cache但是使用Buffer

![image-20201126160018267](pic/06_内存映射/01_default_mem_map.png)



Liteos-a的地址空间是怎么分配的？
`KERNEL_VMM_BASE`等于0x40000000，并且在`kernel\liteos_a\kernel\base\include\los_vm_zone.h`看到如下语句：

```c
#if (PERIPH_UNCACHED_BASE >= (0xFFFFFFFFU - PERIPH_UNCACHED_SIZE))
#error "Kernel virtual memory space has overflowed!"
#endif
```

所以可以粗略地认为：

* 内核空间：0x40000000 ~ 0xFFFFFFFF
* 用户空间：0 ~ 0x3FFFFFFF