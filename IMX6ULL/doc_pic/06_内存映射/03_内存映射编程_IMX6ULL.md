# 内存映射编程_IMX6ULL

## 1.1 最终结果

本章节做的修改会制作为补丁文件：

* `02_openharmony_memmap_imx6ull.patch`

先打补丁：openharmony_100ask_v1.2.patch，
再打补丁：01_openharmony_add_demo_board.patch
最后打补丁：02_openharmony_memmap_imx6ull.patch

注意：也许你还会看到其他单板的补丁文件，比如`02_openharmony_memmap_stm32mp157.patch`，不能同时打，因为都是使用vendor/democom/demochip里的源码，同时只能支持一款芯片

假设目录`openharmony`中是未修改的代码，从没打过补丁；
假设补丁文件放在openharmony的同级目录；
打补丁方法如下：

```
$ cd openharmony
$ patch -p1 < ../openharmony_100ask_v1.2.patch
$ patch -p1 < ../01_openharmony_add_demo_board.patch
$ patch -p1 < ../02_openharmony_memmap_imx6ull.patch 
```

打上补丁后，可以如此编译：

```
$ cd kernel/liteos_a
$ cp tools/build/config/debug/demochip_clang.config .config
$ make clean
$ make
```

## 1.2 现场编程

参考资料：`IMX6ULLRM.pdf`

### 1.2.1 内存地址范围

![image-20201126214048323](pic/06_内存映射/09_imx6ull_mem_ddr_map.png)

100ASK_IMX6ULL开发板上DDR容量是512M，所以：

```c
// vendor\democop\demochip\board\include\board.h
#define DDR_MEM_ADDR            0x80000000
#define DDR_MEM_SIZE            0x20000000
```

### 1.2.2 设备地址范围

IMX6ULL芯片上设备地址分部太零散，从0到0x6FFFFFFF都有涉及，中间有很多保留的地址不用，入下图：

![image-20201126214724302](pic/06_内存映射/10_imx6ull_device_map.png)如果把0到0x6FFFFFFF全部映射完，地址空间不够；
**正确的做法**应该是忽略那些保留的地址空间，为各个模块**单独映射地址**。
但是Liteos-a尚未实现这样的代码(要自己实现也是可以的，但是我们先把最小系统移植成功)。
我们至少要映射2个设备的地址：UART1(100ASK_IMX6ULL开发板使用UART1)、GIC，如下图：
![image-20201126215916943](pic/06_内存映射/11_imx6ull_gic_map.png)

![image-20201126220015659](pic/06_内存映射/12_imx6ull_uart1_map.png)

所以：

```c
// source\vendor\democom\demochip\board\include\board.h
#define PERIPH_PMM_BASE         0x00a00000   // GIC的基地址
#define PERIPH_PMM_SIZE         0x02300000   // 尽可能大一点,以后使用其他外设时就不用映射了
```

**PERIPH_PMM_SIZE**也不能太大，限制条件是：

```c
#if (PERIPH_UNCACHED_BASE >= (0xFFFFFFFFU - PERIPH_UNCACHED_SIZE))
#error "Kernel virtual memory space has overflowed!"
#endif
```

