# 正式版本的init进程

Liteos-a中有两个init程序：

* 测试版本：`kernel\liteos_a\apps\init\src\init.c`
* 正式版本：`base\startup\services\init_lite\src\main.c`

## 1.1 测试版本

源码：`kernel\liteos_a\apps\init\src\init.c`
我们在`kernel\liteos_a`目录下执行`make rootfs`时使用的就是测试版本，
它的功能很简单：只是启动`/bin/shell`程序，源码如下：

```c
int main(int argc, char * const *argv)
{
    int ret;
    const char *shellPath = "/bin/shell";

    ret = fork();
    if (ret < 0) {
        printf("Failed to fork for shell\n");
    } else if (ret == 0) {
        (void)execve(shellPath, NULL, NULL);
        exit(0);
    }

    while (1) {
        ret = waitpid(-1, 0, WNOHANG);
        if (ret == 0) {
            sleep(1);
        }
    };
}
```



## 1.2 正式版本

源码：`base\startup\services\init_lite\src\main.c`

![image-20201206213721158](pic/10_根文件系统/01_init_main.c.png)

怎么单独编译正式版本，尚未研究。
可以使用这样的命令去编译：`python build.py ipcamera_hi3518ev300 -b debug`
可以得到rootfs目录，里面有`/bin/init, /etc/init.cfg`等文件。

### 1.2.1 配置文件

#### 1. 分析配置文件

配置文件中内容分为两部分：

* services：定义了多个服务，它对应某些APP
* jobs：可以定义一些APP，也可去启动服务
  * pre-init：预先执行的初始化
  * init：初始化
  * post-init：最后的初始化

#### 2. 示例

```
./vendor/huawei/camera/init_configs/init_liteos_a_3516dv300.cfg
./vendor/huawei/camera/init_configs/init_liteos_a_3518ev300.cfg
```



#### 3. 配置文件执行过程

视频里讲解。


