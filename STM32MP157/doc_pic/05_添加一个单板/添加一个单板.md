# 添加一个单板

按照编译内核的流程，添加代码。
补丁文件`openharmony_100ask_v1.2.patch`，已经添加了2个单板：STM32MP157、IMX6ULL。
目标：仿照它们，添加一个新的单板，以后修改源码让它再次支持STM32MP157或IMX6ULL。
毕竟我们是教学，从0写代码比较好。
新加的单板：

* 芯片公司：DemoCom

* 芯片名称：DemoChip

本章节做的修改会制作为补丁文件：01_openharmony_add_demo_board.patch，
先打补丁：openharmony_100ask_v1.2.patch
再打补丁：01_openharmony_add_demo_board.patch

假设目录`openharmony`中是未修改的代码，从没打过补丁；
假设补丁文件放在openharmony的同级目录；
打补丁方法如下：

```
$ cd openharmony
$ patch -p1 < ../openharmony_100ask_v1.2.patch
$ patch -p1 < ../01_openharmony_add_demo_board.patch
```

打上补丁后，可以如此编译：

```
$ cd kernel/liteos_a
$ cp tools/build/config/debug/demochip_clang.config .config
$ make clean
$ make
```



## 1.1 配置界面里添加单板

kernel/liteos_a/platform/Kconfig

```
LOSCFG_PLATFORM="demochip"
# LOSCFG_PLATFORM_HI3516DV300 is not set
# LOSCFG_PLATFORM_HI3518EV300 is not set
# LOSCFG_PLATFORM_STM32MP157 is not set
# LOSCFG_PLATFORM_IMX6ULL is not set
LOSCFG_PLATFORM_DEMOCHIP=y
```





## 1.2 添加源码

在vendor目录下创建：DemoCom/DemoChip目录，里面放置文件。



## 1.3 Makefile中根据配置项修改

Makefile
platform/Makefile
platform/bsp.mk

## 1.4 能否编译通过

shell/full/src/base/show.c

shell/full/src/cmds/dmesg.c

shell/full/src/base/shcmd.c

shell/full/src/base/shmsg.c



## 1.5 解决链接错误

tools/build/mk/los_config.mk 

mv vendor/st/stm32mp157/stm32mp157.mk vendor/st/stm32mp157/demochip.mk

vendor/democom/hdf/hdf_vendor.mk

drivers/hdf/lite/hdf_lite.mk



## 1.6 内核启动流程

内核启动流程可以分为4步骤(非官方)：

* 启动
  * 使用汇编代码编写，涉及非常底层的设置，比如CPU设置、代码重定位等等
  * 地址映射也在这里实现
  * 它最终会调用main函数

* main函数
  * 以后的代码，基本都是使用C语言编写了
  * 主要工作是：调用OsMain进行各类初始化、最终会启动用户程序
* OsMain函数
  * 进行操作系统层面的初始化，比如异常初始化、任务初始化、IPC初始化
  * 调用SystemInit

* SystemInit
  * 偏向于应用程序的初始化
  * 挂载根文件系统
  * 启动第一个用户进程

### 1.6.1 启动文件分析

从`kernel\liteos_a\arch\arm\arm\src\startup\reset_vector_up.S`开始阅读代码，
流程如下：

```flow
st=>start: start
cpu_init=>operation: 禁止中断/设置CPU为ARM模式
cache_disable=>operation: 禁止CACHE
relocate=>operation: 代码重定位
mmu_init=>operation: 设置页表(虚拟地址/物理地址映射)
stack_init=>operation: 设置栈
vector_set=>operation: 设置向量表基地址
clear_bss=>operation: 清除BSS
main=>operation: 调用main函数
e=>end: end
st->cpu_init->cache_disable->relocate->mmu_init->stack_init->vector_set->clear_bss
clear_bss->main->e
```

### 1.6.2 main函数分析

main函数在这个文件里`kernel\liteos_a\platform\main.c`，
流程如下：

```flow
st=>start: start
main=>operation: main
OsSetMainTask=>operation: OsSetMainTask
OsCurrTaskSet=>operation: OsCurrTaskSet
OsSystemInfo=>operation: 打印系统信息
OsMain=>operation: OsMain
OsStart=>operation: 启动操作系统(开始调度APP)
e=>end: end
st->main->OsSetMainTask->OsCurrTaskSet->OsSystemInfo->OsMain->OsStart->e
```

### 1.6.3 OsMain函数分析
OsMain函数在这个文件里`kernel\liteos_a\kernel\common\los_config.c`，
流程如下：

```flow
st=>start: start
OsMain=>operation: OsMain
others_init1=>operation: 各类初始化1
OsTickInit=>operation: OsTickInit(会调用到单板的HalClockInit)
others_init2=>operation: 各类初始化2
OsSystemInit=>operation: OsSystemInit(会创建任务进而调用SystemInit)
others_init3=>operation: 各类初始化3
e=>end: end
st->OsMain->others_init1->OsTickInit->others_init2->OsSystemInit->others_init3->e
```
### 1.6.4 SystemInit函数分析
SystemInit函数在这个文件里`vendor\st\stm32mp157\board\board.c`，
流程如下：

```flow
st=>start: start
SystemInit=>operation: SystemInit
mount_rootfs=>operation: 初始化Flash/挂载根文件系统
uart_dev_init=>operation: 初始化串口驱动(给APP使用)
virtual_serial_init=>operation: 初始化虚拟串口驱动(给APP使用)
system_console_init=>operation: system_console_init
OsUserInitProcess=>operation: 创建第一个用户进程
e=>end: end
st->SystemInit->mount_rootfs->uart_dev_init->virtual_serial_init->system_console_init->OsUserInitProcess->e
```