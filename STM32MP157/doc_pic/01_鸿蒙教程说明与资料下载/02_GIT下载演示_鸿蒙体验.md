# 1. GIT上的文档是学习的入口

所以，第一件事是去下载GIT资料。

先去https://gitforwindows.org/下载Windows版本的git工具。
小技巧：如果下载太慢，可以在百度搜`Git-2.29.2-64-bit.exe`。

安装、启动Git Bash。

然后执行命令：
```
git clone  https://e.coding.net/weidongshan/openharmony/doc_and_source_for_openharmony.git
```



# 2. 下载安装VMWare及打开Ubuntu镜像

要开发，需要使用VMWare运行Ubuntu，怎么做？

## 2.1 可以看文档

![](pic\01_introduction _and_download\006_doc_in_git.png)

## 2.2 可以看视频

打开www.100ask.net，可以免费观看视频，跟嵌入式Linux的开发环境是一样的。

![](pic\01_introduction _and_download\007_video_for_base_tools.png)



# 3. 鸿蒙体验