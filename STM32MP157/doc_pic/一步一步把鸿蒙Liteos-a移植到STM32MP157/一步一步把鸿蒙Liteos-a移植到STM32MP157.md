[toc]


# 1. 准备工作与说明

## 1.1 准备工作

请先下载以下文件，里面有文档：

https://gitee.com/weidongshan/openharmony_for_imx6ull/repository/archive/master.zip

参考GIT中的文档，安装好虚拟机，从GIT中下载Liteos-a源码，打上IMX6ULL的补丁。

## 1.2 说明

本文档是基于IMX6ULL的鸿蒙源码，一步一步移植STM32MP157。
只适合有基础、动手能力强的人。
本文档尽量把移植过程中要做的事情记录下来，但是无法把每一步都记录下来。
所以，如果你很可能碰到一些问题，而在文档中没有提及。
如果你想更全面地学习，请从www.100ask.net观看鸿蒙移植视频。

本文档可以使用Typora打开。

## 1.3 适用板子

百问网STM32MP157, IMX6ULL

# 2. Makefile编译系统分析

## 2.1 从最终的链接命令看Liteos-a的组成

* 链接命令如下

  liteos-a由一系列的库文件组成，reset_vector是它的入口。

![link_liteos-a](pic\001_liteos-a_link.png)
* 查看链接脚本

  ![liteos_llvm.ld](pic\002_liteos_llvm.ld.png)

* 查看liteos.map文件

  <img src="pic\003_liteos_map.png" alt="liteos.map" style="zoom:150%;" />

## 2.2 Makfile中常用变量

1. LITEOSTOPDIR                        // kernel/liteos_a
2. LITEOSTHIRDPARTY               // third_party
3. LITEOS_MK_PATH                   // kernel/liteos_a/tools/build/mk
4. MK_PATH  = $(LITEOSTOPDIR)/tools/build/mk       // kernel/liteos_a/tools/build/mk

## 2.3 包含的文件
Makefile
	-include $(LITEOSTOPDIR)/tools/build/config.mk
		-include $(LITEOSTOPDIR)/tools/build/mk/los_config.mk
			-include $(LITEOSTOPDIR)/.config
			include $(LITEOSTOPDIR)/arch/cpu.mk
				-include $(LITEOSTOPDIR)/arch/arm/arm.mk
			include $(LITEOSTOPDIR)/platform/bsp.mk
			include $(LITEOSTOPDIR)/../../vendor/nxp/imx6ull/imx6ull.mk
			include $(LITEOSTOPDIR)/../../drivers/hdf/lite/hdf_lite.mk
				include $(LITEOSTOPDIR)/../../vendor/nxp/hdf/hdf_vendor.mk
			-include $(LITEOSTOPDIR)/3rdParty/3rdParty.mk
		-include $(LITEOS_MK_PATH)/liteos_tables_ldflags.mk
	-include $(LITEOS_MK_PATH)/dynload.mk

## 2.4 把Makefile全部展开
得到了一个Makefile_all.txt，
分析Makefile_all.txt，就可以知道编译过程。

## 2.5 分析在kernel/liteos_a下执行make的过程
### 2.5.1 第1个目标
all: $(OUT) $(BUILD) $(LITEOS_TARGET) $(APPS)

### 2.5.2 目标：OUT
* OUT目标：

  创建了目录：kernel/liteos_a/imx6ull/lib

  把board.ld.S编译成了board.ld，这是链接文件。
```
# .config文件中， LOSCFG_PLATFORM="imx6ull"
OUT  = $(LITEOSTOPDIR)/out/$(LITEOS_PLATFORM)

$(OUT): $(LITEOS_MENUCONFIG_H)
	$(HIDE)mkdir -p $(OUT)/lib
	$(HIDE)$(CC) -I$(LITEOS_PLATFORM_BASE)/include -I$(BOARD_INCLUDE_DIR) \
		-E $(LITEOS_PLATFORM_BASE)/board.ld.S \
		-o $(LITEOS_PLATFORM_BASE)/board.ld -P
```
* OUT的依赖：LITEOS_MENUCONFIG_H

  配置内核，生成头文件autoconf.h。
```
LITEOS_MENUCONFIG_H = $(LITEOSTOPDIR)/include/generated/autoconf.h

KCONFIG_FILE_PATH = $(LITEOSTOPDIR)/Kconfig

$(LITEOS_MENUCONFIG_H):
ifneq ($(LITEOS_PLATFORM_MENUCONFIG_H), $(wildcard $(LITEOS_PLATFORM_MENUCONFIG_H)))
	$(HIDE)$(MAKE) genconfig
endif

genconfig:$(MENUCONFIG_PATH)/conf
	$(HIDE)mkdir -p include/config include/generated
	$< --silentoldconfig $(KCONFIG_FILE_PATH)
	-mv -f $(LITEOS_MENUCONFIG_H) $(LITEOS_PLATFORM_MENUCONFIG_H)
```
### 2.5.3 目标：BUILD
创建目录 kernel/liteos_a/imx6ull/obj

```
OUT  = $(LITEOSTOPDIR)/out/$(LITEOS_PLATFORM)
BUILD  = $(OUT)/obj
$(BUILD):
	$(HIDE)mkdir -p $(BUILD)
```

### 2.5.4 目标：LITEOS_TARGET

这是核心，进入子目录执行make，把子目录中的文件链接为一个库。

最后，把这些库链接为liteos内核。

```
LITEOS_TARGET = liteos
$(LITEOS_TARGET): $(__LIBS)
	$(HIDE)touch $(LOSCFG_ENTRY_SRC)

	$(HIDE)for dir in $(LITEOS_SUBDIRS); \
	do $(MAKE) -C $$dir all || exit 1; \
	done

	$(LD) $(LITEOS_LDFLAGS) $(LITEOS_TABLES_LDFLAGS) $(LITEOS_DYNLDFLAGS) -Map=$(OUT)/$@.map -o $(OUT)/$@ --start-group $(LITEOS_LIBDEP) --end-group
#	$(SIZE) -t --common $(OUT)/lib/*.a >$(OUT)/$@.objsize
	$(OBJCOPY) -O binary $(OUT)/$@ $(LITEOS_TARGET_DIR)/$@.bin
	$(OBJDUMP) -t $(OUT)/$@ |sort >$(OUT)/$@.sym.sorted
	$(OBJDUMP) -d $(OUT)/$@ >$(OUT)/$@.asm
#	$(NM) -S --size-sort $(OUT)/$@ >$(OUT)/$@.size
```
* 目标：__LIBS

  ```
  # 没做什么事
  __LIBS = libs
  $(__LIBS): $(OUT) $(CXX_INCLUDE)
  ```

  

* 命令：$(HIDE)touch $(LOSCFG_ENTRY_SRC)

  每次都要编译los_config.c，touch一下

  ```
  LOSCFG_ENTRY_SRC    = $(LITEOSTOPDIR)/kernel/common/los_config.c
  ```

* 命令：进入每个LITEOS_SUBDIRS，执行make，后面重点讲解各个子目录的编译

  ```
  # LIB_SUBDIRS 等于一系列的目录
  LIB_SUBDIRS :=
  LIB_SUBDIRS             += arch/arm/$(LITEOS_ARCH_ARM)  # 就是arch/arm/arm
  LIB_SUBDIRS             += $(PLATFORM_BSP_HISI_BASE)
  LIB_SUBDIRS     += $(LITEOSTOPDIR)/kernel/common
  LIB_SUBDIRS       += kernel/base
  LIB_SUBDIRS     += $(IMX6ULL_BASE_DIR)/board
  LIB_SUBDIRS     += $(IMX6ULL_BASE_DIR)/driver/mtd/common
  LIB_SUBDIRS     += $(IMX6ULL_BASE_DIR)/driver/mtd/spi_nor
  LIB_SUBDIRS             += $(IMX6ULL_BASE_DIR)/driver/imx6ull-fb
  LIB_SUBDIRS             += $(IMX6ULL_BASE_DIR)/driver/imx6ull-uart
  LIB_SUBDIRS         += kernel/extended/cpup
  LIB_SUBDIRS        += lib/libc
  LIB_SUBDIRS        += lib/libsec
  LIB_SUBDIRS         += lib/libscrew
  LIB_SUBDIRS     += fs/fat
  LIB_SUBDIRS     += fs/jffs2
  
  LITEOS_SUBDIRS   = $(LIB_SUBDIRS)
  
  $(HIDE)for dir in $(LITEOS_SUBDIRS); \
  	do $(MAKE) -C $$dir all || exit 1; \
  	done
  ```
* 链接及各类处理
```
	$(LD) $(LITEOS_LDFLAGS) $(LITEOS_TABLES_LDFLAGS) $(LITEOS_DYNLDFLAGS) -Map=$(OUT)/$@.map -o $(OUT)/$@ --start-group $(LITEOS_LIBDEP) --end-group
#	$(SIZE) -t --common $(OUT)/lib/*.a >$(OUT)/$@.objsize
	$(OBJCOPY) -O binary $(OUT)/$@ $(LITEOS_TARGET_DIR)/$@.bin
	$(OBJDUMP) -t $(OUT)/$@ |sort >$(OUT)/$@.sym.sorted
	$(OBJDUMP) -d $(OUT)/$@ >$(OUT)/$@.asm
#	$(NM) -S --size-sort $(OUT)/$@ >$(OUT)/$@.size
```

### 2.5.5 怎么编译各个子目录

以kernel/liteos_a/fs/fat/Makefile为例：

![kernel/liteos_a/fs/fat/Makefile](pic\004_fat_makefile.png)

* 第1行包含config.mk
  
   这是包含一些预先定义的变量，比如默认的编译选项等。
   
* 定义了LOCAL_SRCS
  
   等于一系列C文件，这就是要编译的源文件。
   
* 定义了LOCAL_INCLUDE

   这是头文件的目录
   
* 定义了LOCAL_FLAGS

   这是编译选项

* 定义了MODULE_NAME
  
  一般等于当前目录的名字，比如fat，以后就编译得到libfat.a
  
* 怎么编译？看最后一行

   ```
   include $(MODULE)
   ```

   MODULE就是：

   ```
   MODULE = $(MK_PATH)/module.mk  # kernel/liteos_a/tools/build/mk/module.mk
   ```

   分析module.mk：

   ```
   # 找到第1个目标
   all : $(LIB)
   
   # LIB是什么, 如果没定义LOCAL_SO，LIB就是 lib$(MODULE_NAME).a, 比如 libfat.a
   ifeq ($(LOCAL_SO), y)
   LIBSO := $(OUT)/lib/lib$(MODULE_NAME).so
   LIBA := $(OUT)/lib/lib$(MODULE_NAME).a
   else
   LIBSO :=
   LIBA := $(OUT)/lib/lib$(MODULE_NAME).a
   endif
   LIB := $(LIBA) $(LIBSO)
   
   # 怎么编译 LIBA ? 看下图
   ```


![module](pic\005_module.png)



# 3. 添加STM32MP157单板

* 技巧1：

​       STM32MP157跟HI3516DV300都属于cortex A7 smp架构，可以搜索HI3516DV300并仿照它添加文件。

​		可以搜索“PLATFORM_HI3516DV300”，仿照它添加“PLATFORM_STM32MP157”。

​		只涉及2个文件：kernel/liteos_a/Kconfig、kernel/liteos_a/platform/Kconfig。

* 技巧2：

  海思芯片对应的源码在Liteos-a中并不开源，所以还需要参考IMX6ULL的代码。

## 3.1 在配置菜单Kconfig中添加STM32MP157

* 修改：kernel/liteos_a/Kconfig

![add_stm32mp157_kconfig](pic\006_add_stm32mp157_kconfig.png)

* 修改 kernel/liteos_a/platform/Kconfig

![add_stm32mp157_platform_kconfig](pic\007_add_stm32mp157_platform_kconfig.png)

## 3.2 添加STM32MP157的默认配置文件

* 配置文件
	位于 kernel/liteos_a/tools/build/config/debug。
	
	名字里含有clang的配置文件，是使用clang工具链；
	
	名字里不含有clang的配置文件，是使用GNU工具链。
	
	Liteos-a默认使用clang工具链。

![default_config_file](pic\008_default_config_file.png)

* 添加STM32MP157的配置文件
	仿照hi3516dv300_clang.config和imx6ull_clang.config添加。
```
cp  imx6ull_clang.config  stm32mp157_clang.config
```

​		再作如下修改：

![stm32mp157_clang.config](pic\009_stm32mp157_clang.config.png)



## 3.3 添加单板相关的代码

* 参考IMX6ULL来添加

  IMX6ULL单板相关的文件位于这个目录：vendor/nxp，内容如下：

  ```
  vendor
  └── nxp
      ├── hdf
      │   └── hdf_vendor.mk
      └── imx6ull
          ├── board
          │   ├── board.c
          │   ├── bsd_board.c
          │   ├── include
          │   │   ├── asm
          │   │   │   ├── hal_platform_ints.h
          │   │   │   └── platform.h
          │   │   ├── board.h
          │   │   ├── clock.h
          │   │   ├── platform_config.h
          │   │   ├── reset_shell.h
          │   │   ├── spinor.h
          │   │   └── uart.h
          │   └── Makefile
          ├── config
          │   ├── device_info
          │   │   └── device_info.hcs
          │   ├── hdf.hcs
          │   ├── i2c
          │   │   └── i2c_config.hcs
          │   └── Makefile
          ├── driver
          │   ├── hello
          │   │   ├── hello_drv.c
          │   │   └── Makefile
          │   ├── imx6ull-fb
          │   │   ├── imx6ull_lcd.c
          │   │   ├── imx6ull_lcdc.c
          │   │   ├── imx6ull_lcdc.h
          │   │   ├── imx6ull_lcd.h
          │   │   └── Makefile
          │   ├── imx6ull-i2c
          │   │   ├── i2c_dev.c
          │   │   ├── i2c_dev.h
          │   │   ├── i2c_imx6ull.c
          │   │   └── Makefile
          │   ├── imx6ull-uart
          │   │   ├── imx6ull_uart.h
          │   │   ├── Makefile
          │   │   ├── uart_core.c
          │   │   ├── uart_dev.c
          │   │   ├── uart_dev.h
          │   │   └── uart_imx6ull.c
          │   ├── mtd
          │   │   ├── common
          │   │   │   ├── BUILD.gn
          │   │   │   ├── include
          │   │   │   │   ├── hifmc_common.h
          │   │   │   │   ├── mtd_common.h
          │   │   │   │   └── spi_common.h
          │   │   │   ├── Makefile
          │   │   │   └── src
          │   │   │       ├── common.c
          │   │   │       ├── mtdblock.c
          │   │   │       ├── mtdchar.c
          │   │   │       └── mtd_list.c
          │   │   └── spi_nor
          │   │       ├── BUILD.gn
          │   │       ├── include
          │   │       │   └── spinor.h
          │   │       ├── Kconfig
          │   │       ├── Makefile
          │   │       └── src
          │   │           └── common
          │   │               ├── host_common.h
          │   │               ├── ramdisk.c.bak2
          │   │               ├── ramdisk.c.ok
          │   │               ├── spinor.c
          │   │               └── spinor_common.h
          │   └── touch
          │       ├── Makefile
          │       ├── touch_gt9xx.c
          │       └── touch_gt9xx.h
          └── imx6ull.mk
  ```

* 复制修改单板文件

  注意，这样添加的代码肯定无法用在STM32MP157上，以后还需要改代码。
  
  先复制再改名，如下图操作：

![add_stm32mp157_board_file](pic\010_add_stm32mp157_board_file.png)

* 修改Makefile

    在vendor/st目录下，执行以下命令：

  ```
  grep "imx6ull" * -nr | grep Makefile
  ```

  找到几个Makefile，把里面的”imx6ull“全部改名为"stm32mp157"，如下图：

![change_makefile_of_vendor_st](pic\011_change_makefile_of_vendor_st.png)



## 3.4 修改Makefile

我们提供了vendor/st目录，也修改了目录里面的Makefile，但是还需要修改其他Makefile，才会去编译这目录。

执行以下搜索命令：
```
grep "LOSCFG_PLATFORM_HI3516DV300" * -nwr
```

在得到的文件中，添加STM32MP157的项目，只涉及3个文件：

![modify_makefile_to_build_stm32mp157](pic\012_modify_makefile_to_build_stm32mp157.png)

### 3.4.1 修改kernel/liteos_a/Makefile

在顶层Makefile中，确定STM32MP157的文件系统类型，指定STM32MP157的单板源码路径。

![modify_kernel_liteos_a_Makefile](pic\013_modify_kernel_liteos_a_Makefile.png)

### 3.4.2 修改kernel/liteos_a/platform/Makefile

修改它，在使用STM32MP157是，也要编译kernel/common下的文件，这些是通用的文件。

![modify_kernel_liteos_a_platform_Makefile](pic\014_modify_kernel_liteos_a_platform_Makefile.png)

### 3.4.3 修改kernel/liteos_a/platform/bsp.mk

这涉及的是定时器的代码，单板相关的头文件目录。

![modify_kernel_liteos_a_platform_bsp.mk](pic\015_modify_kernel_liteos_a_platform_bsp.mk.png)

## 3.5 配置内核/编译/必定出错

肯定是会出错的，我们要做到是：根据启动流程、根据出错信息，反复修改并测试。

```
cd kernel/liteos_a
make clean
cp tools/build/config/debug/stm32mp157_clang.config .config
make  // 会提示你，让你选择很多配置项, ctrl + c退出
make menuconfig  // 马上退出，选择保存
cp  .config  tools/build/config/debug/stm32mp157_clang.config  // 配置文件就完整了
make  // 必定出错
```

* 题外话

  配置文件中，比如你配置了LOSCFG_PLATFORM_STM32MP157，

  但是不需要配置 LOSCFG_PLATFORM_HI3516DV300，

  在.config中，LOSCFG_PLATFORM_HI3516DV300也要写出来(写为"not set")，如下：

  ```
  LOSCFG_PLATFORM="stm32mp157"
  # LOSCFG_PLATFORM_HI3518EV300 is not set
  LOSCFG_PLATFORM_STM32MP157=y
  # LOSCFG_PLATFORM_HI3516DV300 is not set
  ```

  如果不写的话，make时就会提示：

  ```
  *
  * Restart config...
  *
  *
  * Platform
  *
  Board
    1. hi3516dv300 (PLATFORM_HI3516DV300) (NEW)
    3. hi3518ev300 (PLATFORM_HI3518EV300)
  > 3. stm32mp157 (PLATFORM_STM32MP157)
    4. imx6ull (PLATFORM_IMX6ULL)
  choice[1-4?]:
  ```

  所以，我们第一次配置时，干脆执行"make menuconfig"并立刻退出并保存，

  这会帮助我们在.config中把没用到的配置项也作为"# LOSCFG_XXX is not set"列出来。

  为防止下次配置时，仍然碰到这个问题，执行：

  ```
  cp  .config  tools/build/config/debug/stm32mp157_clang.config
  ```

  

# 4. 编译/出错/修改

最苦逼、最有技术含量的活儿开始了。
编译通过后，从kernel/liteos_a/arch/arm/arm/src/startup/reset_vector_mp.S一路跟踪，
反复进行：修改、编译、下载运行测试
本章节先修改代码，把IMX6UL的宏开关都修改为STM32MP157，能在`kernel/liteos-a`目录编译成功。
本节目标只是能编译成功，肯定无法在板子上运行：因为代码都是IMX6ULL的，尚未根据STM32MP157来修改。

## 4.1 PERI_CRG30_BASE未定义

![](pic\016_PERI_CRG30_BASE_undef.png)

* 这是hi3516dv300的源码，目的是`clear the slave cpu reset`
* 对于STM32MP157，我们要使用另外的代码

* 先使用UP来调通吧
  * 如下配置内核：
    ![](pic\017_not_use_smp.png)
  * make clean && make



## 4.2 'hisoc/uart.h' file not found

![](pic\018_board_head_file_uart.h.png)

涉及文件：

* kernel/liteos_a/shell/full/src/base/shcmd.c
* kernel/liteos_a/shell/full/src/base/show.c
* kernel/liteos_a/shell/full/src/base/shmsg.c
* kernel/liteos_a/shell/full/src/cmds/dmesg.c

## 4.3 链接错误

错误如下：

![](pic\019_link_err_mtd_common_not_found.png)

原因：vendor目录下的文件没有编译。



### 4.3.1 修改mk文件增加vendor

修改`kernel/liteos_a/tools/build/mk/los_config.mk`

![](pic\020_modify_mk_to_build_vendor.png)



* 修改`vendor/st/stm32mp157/stm32mp157.mk`
  把IMX6ULL改为STM32MP157，包括目录名
* 编译、出错、修改：基本就是头文件找不到、宏未定义
  * 头文件找不到：修改Makefile，在`LOCAL_FLAGS`中增加`-I dir`
  
  * 宏未定义：参考IMX6ULL，添加宏
    示例：`kernel/liteos_a/kernel/base/include/los_vm_zone.h`
  
    ![](pic\021_add_macro_like_imx6ull.png)
  
### 4.3.2 添加vendor/st后

又出现链接错误，如下：

![](pic\022_link_err_GetDevSpiNorOps.png)

解决方法：`make clean && make`

### 4.3.3 HdfGetBuildInConfigData undefined

![](pic\023_link_err_HdfGetBuildInConfigData.png)

解决方法：

* 修改`drivers/hdf/lite/hdf_lite.mk`

  ![](pic\024_hdf_lite.mk.add_stm32mp157.png)

* 修改`drivers/hdf/lite/hdf_driver.mk`

![](pic\025_hdf_driver.mk.add_stm32mp157.png)

* 修改`vendor/st/hdf/hdf_vendor.mk`:



# 5. 测试/出错/修改

思路：根据liteos启动流程来调试。

## 5.1 在程序设置MMU之前打印字符

目的：确认程序能执行。

### 5.1.1 修改源码

需要实现一个uart_phy_putc，在`vendor/st/stm32mp157/driver/stm32mp157-uart/uart_stm32mp157.c`中：

```c
void uart_putc_phy(unsigned char c)
{
	UART_Type *uartRegs = (UART_Type *)UART4_REG_PBASE;
	while ((uartRegs->USART_ISR & (1<<7)) == 0);
	uartRegs->USART_TDR = c;
}
```

调用它，在`kernel/liteos_a/arch/arm/arm/src/startup/reset_vector_up.S`中设置栈后，就可以调用了：

```c
reset_vector:
	ldr sp, =0xc0000000 + 0x1000000
	mov r0, #'S'
	bl uart_putc_phy
```

### 5.1.2 编译

对于STM32MP157，需要使用mkimage在liteos.bin前面添加头部，这样STM32CubeProgrammer才可以直接启动它。

这个mkimage来自STM32MP157是u-boot，它跟其他u-boot自带的mkimage不一样。我提前把它放到的`vendor/st/tools`下。

修改`kernel/liteos_a/Makefile`，添加如下代码：
![](pic\026_mkimage_liteos.stm32.png)

```
$(LITEOS_TARGET): $(__LIBS)
        $(HIDE)touch $(LOSCFG_ENTRY_SRC)

        $(HIDE)for dir in $(LITEOS_SUBDIRS); \
        do $(MAKE) -C $$dir all || exit 1; \
        done

        $(LD) $(LITEOS_LDFLAGS) $(LITEOS_TABLES_LDFLAGS) $(LITEOS_DYNLDFLAGS) -Map=$(OUT)/$@.map -o $(OUT)/$@ --start-group $(LITEOS_LIBDEP) --end-group
#       $(SIZE) -t --common $(OUT)/lib/*.a >$(OUT)/$@.objsize
        $(OBJCOPY) -O binary $(OUT)/$@ $(LITEOS_TARGET_DIR)/$@.bin
ifeq ($(LOSCFG_PLATFORM_STM32MP157), y)
        chmod +x $(LITEOSTOPDIR)/../../vendor/st/tools/mkimage
        $(LITEOSTOPDIR)/../../vendor/st/tools/mkimage -T stm32image -a 0xC0100000 -e 0xC0100000 -d $(LITEOS_TARGET_DIR)/$@.bin $(LITEOS_TARGET_DIR)/liteos.stm32
endif
        $(OBJDUMP) -t $(OUT)/$@ |sort >$(OUT)/$@.sym.sorted
        $(OBJDUMP) -d $(OUT)/$@ >$(OUT)/$@.asm
```



### 5.1.3 运行、观察串口信息

STM32MP157提供了一个工具：STM32CubeProgrammer，使用它可以把程序下载到内存直接运行，也可以烧写到emmc上。
为方便，我们使用它下载到内存直接运行。
需要提供一个`openharmony_liteos_ram.tsv`文件，内容如下：
```
#Opt	Id	Name	Type	IP	Offset	Binary
-	0x01	fsbl1-boot	Binary	none	0x0	Flashlayout/tf-a-stm32mp157c-100ask-512d-v1-serialboot.stm32
-	0x03	ssbl-boot	Binary	none	0x0	Flashlayout/liteos.stm32
```

可以看到，需要借助一个TFA程序(`tf-a-stm32mp157c-100ask-512d-v1-serialboot.stm32`)来下载运行。
在视频或文档里，会有详细的操作步骤。
![](pic\027_liteos.stm32.uart_putchar_phy.png)



## 5.2 内存映射

目的：reset_vector_up.S中一开始就设置地址映射了，所以这是要调试的第一个问题。

### 5.2.1 涉及的代码

在`kernel/liteos_a/arch/arm/arm/src/startup/reset_vector_up.S`中有一个宏**PAGE_TABLE_SET**，
是用来设置地址映射的： 
![](pic\028_page_table_set.png)

怎么添加内存映射呢？示例如下：

![](pic\029_virt_phy_map.png)



### 5.2.2 确定内存划分

对于内存，我们需要确定它的起始地址。
我们还是要内存来模拟Flash、直接划分LCD用的Framebuffer。
代码在`vendor/st/stm32mp157/board/include/board.h`，如下：

```c
/* physical memory base and size */
#define DDR_MEM_ADDR            0xC0000000
#define DDR_MEM_SIZE            0x18000000   /* reseved 128M for ramfs and framebuffer */

#define DDR_RAMFS_ADDR (DDR_MEM_ADDR + DDR_MEM_SIZE)
#define DDR_RAMFS_SIZE 0x7C00000  /* 64+60M */

#define LCD_FB_BASE    (DDR_RAMFS_ADDR + DDR_RAMFS_SIZE)
#define LCD_FB_SIZE    0x400000  /* 4M */
```

### 5.2.3 外设地址映射

完美的做法是把所有的外设地址都映射了，
但是：liteos-a中的ioremap函数只支持连续空间的地址映射。
就是说如果IOADDR空间A、B不连续的话，ioremap函数无法处理。
所以我们要提供一个连续的IO空间。

* 一般外设的地址空间

![](pic\035_stm32mp157_io_space.png)

根据上图，修改`vendor/st/stm32mp157/board/include/board.h`，如下：

```c
/* Peripheral register address base and size */
#define PERIPH_PMM_BASE         0x40000000
#define PERIPH_PMM_SIZE         0x20000000
```

这2个宏很重要：PERIPH_PMM_BASE、PERIPH_PMM_SIZE，以后驱动程序要访问寄存器时需要使用ioremap：

![](pic\036_ioremap.png)




### 5.2.4 GIC地址空间

除了上图中的外设，还需要GIC(中断控制器)。
需要查看从STM32MP157数据手册。

* GIC基地址
  ![](pic\030_gic_base_addr.png)



GIC的地址空间离PERIPH_PMM_BASE(0x40000000)太远了，单独映射它吧。
修改`vendor/st/stm32mp157/board/include/board.h`，如下：

```c
/* GIC base and size : 1M-align */
#define GIC_PHY_BASE         0xA0000000
#define GIC_PHY_SIZE         0xA0100000
```

上面确定了物理地址，映射到哪里去呢？
修改`kernel/liteos_a/kernel/base/include/los_vm_zone.h`，如下：

```c
#define GIC_VIRT_BASE    (PERIPH_UNCACHED_BASE + PERIPH_UNCACHED_SIZE)
#define GIC_VIRT_SIZE    GIC_PHY_SIZE
```

确定了物理地址、虚拟地址，怎么映射呢？
修改`kernel/liteos_a/arch/arm/arm/src/startup/reset_vector_up.S`，如下：
![](pic\037_gic_map.png)

```c
    PAGE_TABLE_SET SYS_MEM_BASE, UNCACHED_VMM_BASE, UNCACHED_VMM_SIZE, MMU_INITIAL_MAP_STRONGLY_ORDERED
#if defined(LOSCFG_PLATFORM_IMX6ULL) || defined(LOSCFG_PLATFORM_STM32MP157)
    PAGE_TABLE_SET DDR_RAMFS_ADDR, DDR_RAMFS_VBASE, DDR_RAMFS_SIZE, MMU_INITIAL_MAP_DEVICE
    PAGE_TABLE_SET LCD_FB_BASE, LCD_FB_VBASE, LCD_FB_SIZE, MMU_INITIAL_MAP_DEVICE
#endif
    PAGE_TABLE_SET SYS_MEM_BASE, KERNEL_VMM_BASE, KERNEL_VMM_SIZE, MMU_DESCRIPTOR_KERNEL_L1_PTE_FLAGS
    PAGE_TABLE_SET PERIPH_PMM_BASE, PERIPH_DEVICE_BASE, PERIPH_DEVICE_SIZE, MMU_INITIAL_MAP_DEVICE
    PAGE_TABLE_SET PERIPH_PMM_BASE, PERIPH_CACHED_BASE, PERIPH_CACHED_SIZE, MMU_DESCRIPTOR_KERNEL_L1_PTE_FLAGS
    PAGE_TABLE_SET PERIPH_PMM_BASE, PERIPH_UNCACHED_BASE, PERIPH_UNCACHED_SIZE, MMU_INITIAL_MAP_STRONGLY_ORDERED
    PAGE_TABLE_SET GIC_PHY_BASE, GIC_VIRT_BASE, GIC_VIRT_BASE, MMU_INITIAL_MAP_DEVICE
```



### 5.2.5 测试

在`kernel/liteos_a/arch/arm/arm/src/startup/reset_vector_up.S`里，
调用mmu_setup之后，MMU就使能了。
这时可以添加打印，看能否执行到。

* 实现函数uart_putc_virt
  修改`vendor/st/stm32mp157/driver/stm32mp157-uart/uart_stm32mp157.c`

  ```c
  void uart_putc_virt(unsigned char c)
  {
  	UART_Type *uartRegs = (UART_Type *)UART_REG_BASE;
  	while ((uartRegs->USART_ISR & (1<<7)) == 0);
  	uartRegs->USART_TDR = c;
  }
  ```

* 调用函数uart_putc_virt
  ![image-20201026153522700](L:\资料\设计方案\视频教程\hmos\pic\038_test_mmu_ok.png)

* 编译、运行、观察串口信息
  ![](pic\039_test_mmu_run_log.png)



## 5.3 完善串口打印

MMU使能之后，使用串口打印时，就需要用到串口的虚拟地址了。

### 5.3.x UART4中断

* 芯片手册上

![](pic\050_uart4_int.png)



* 代码：

```c
// vendor/st/stm32mp157/board/include/uart.h
#define NUM_HAL_INTERRUPT_UART	  NUM_HAL_INTERRUPT_UART4  /* TODO,100ask */

// vendor/st/stm32mp157/board/include/asm/hal_platform_ints.h
#define NUM_HAL_INTERRUPT_UART4         84

// vendor/st/stm32mp157/board/bsd_board.c
static void uart_add_device(add_res_callback_t callback)
{
    device_t uart_dev;
    UART_ADD_DEVICE(uart_dev, 0);
    callback("uart", SYS_RES_MEMORY, 0, UART4_REG_PBASE,
        UART4_REG_PBASE + UART_IOMEM_COUNT, UART_IOMEM_COUNT);
    callback("uart", SYS_RES_IRQ, 0, NUM_HAL_INTERRUPT_UART4,
        NUM_HAL_INTERRUPT_UART4, 1);

} 
```



## 5.4 确定是否进入了main函数

目的：mmu_setup到main函数之间，还有很多代码，我们直接看看能否执行到main函数。

### 5.4.1 添加打印

修改`kernel/liteos_a/platform/main.c`，如下：
![](L:\资料\设计方案\视频教程\hmos\pic\040_test_main.png)

编译、运行后，发现没有打印上述语句。

### 5.4.2 调试

* 怎么调试？
  使用IDE调试当然是最方便的，但是IDE目前还不支持STM32MP157，所以只能用串口打印。
  类似下图添加代码：
  ![](pic\041_test_before_main.png)

  发现它能打印出来后，就删除掉，再在下一个位置添加打印。
  为什么要删除呢？你可以观看`kernel/liteos_a/out/stm32mp157/liteos.asm`中  uart_putc_virt的反汇编码，
  它用到了r0,r1,r2这3个寄存器，而你添加打印的前后地方可能用到了这3个寄存器。
  为了保险，确定能执行到之后，就删除这些打印语句吧。

  
  
* 调试结果

  STM32MP157是支持FPU+NEON的，也许是使能的方法不对？
  暂时没管它，直接屏蔽代码：

  ![](pic\042_reason_cannot_go_to_main.png)

### 5.4.3 观察串口信息：还是没进入main函数

* 串口信息如下
  ![](pic\043_disable_fpu_neon_log.png)



* 继续添加打印
  在main函数里面添加代码：
![image-20201026170820441](pic\045_print_release_err.png)

* 解决方法

  ![](pic\046_main_ok.png)

### 5.4.4 成功进入main函数

![](pic\047_main_ok_log.png)



## 5.5 GIC设置

在上图信息中，**`GIC Rev     : unknown`**。我们需要指定GIC基地址。

### 5.5.1 代码分析

在哪里读取GIC信息？调用关系如下：

```c
main  // kernel/liteos_a/platform/main.c
    OsSystemInfo  // kernel/liteos_a/platform/main.c
        HalIrqVersion  // kernel/liteos_a/platform/hw/arm/interrupt/gic/gic_v2.c
```

代码如下：

```c
CHAR *HalIrqVersion(VOID) // kernel/liteos_a/platform/hw/arm/interrupt/gic/gic_v2.c
{
    UINT32 pidr = GIC_REG_32(GICD_PIDR2V2);
    CHAR *irqVerString = NULL;

    switch (pidr >> GIC_REV_OFFSET) {
        case GICV1:
            irqVerString = "GICv1";
            break;
        case GICV2:
            irqVerString = "GICv2";
            break;
        default:
            irqVerString = "unknown";
    }
    return irqVerString;
}

// kernel/liteos_a/platform/hw/include/gic_common.h
#define GIC_BASE_ADDR             IO_DEVICE_ADDR(0x00a00000) 

// vendor/st/stm32mp157/board/include\asm/platform.h
#define GICD_PIDR2V2                    (GICD_OFFSET + 0xfe8)

```



### 5.5.2 代码修改

我们在`kernel/liteos_a/arch/arm/arm/src/startup/reset_vector_up.S`中，为GIC单独映射了地址。
不能使用IO_DEVICE_ADDR获得GIC的虚拟地址，
需要单独计算，代码如下：

```c
// vendor/st/stm32mp157/board/include\asm/platform.h
/*------------------------------------------------
 * GIC reg base address
 *------------------------------------------------*/
/* GIC_PHY_BASE(0xA0000000) ==> GIC_VIRT_BASE 
 * GICD(0xA0021000)
 * GICC(0xA0022000)
 * so: GIC_BASE_ADDR = vir addr of 0xA0020000
 */ 
//#define GIC_BASE_ADDR             IO_DEVICE_ADDR(0x00a00000)
#define GIC_BASE_ADDR             (GIC_VIRT_BASE + 0x20000)
```

### 5.5.3 运行

GIC信息成功打印出来了：

![](pic\048_gic_ok_log.png)



## 5.6 系统时钟

liteos-a使用CPU核上的sys tick时钟，对应的代码时通用的：
`kernel/liteos_a/platform/hw/arm/timer/arm_generic/arm_generic_timer.c`

我们只需要指定中断号：
![](pic\049_HalClockInit.png)

这个中断号不用改，STM32MP157跟IMX6ULL同属于cortex A7，这个中断号是一样的。

代码在`vendor/st/stm32mp157/board/include/asm/hal_platform_ints.h`：

```c
#define NUM_HAL_INTERRUPT_CNTPSIRQ      29
#define NUM_HAL_INTERRUPT_CNTPNSIRQ     30
#define OS_TICK_INT_NUM                 NUM_HAL_INTERRUPT_CNTPSIRQ // use secure physical timer for now
```



## 5.7 文件系统

我们使用内存来模拟Flash，代码已经写好，跟IMX6ULL是一样的。
函数调用流程如下：

```c

```

## 5.8 去掉I2C

vendor/st$ vi hdf/hdf_vendor.mk



## 5.9 进入系统

![](pic\051_enter_system.png)



## 5.10 串口上一输入

![](pic\052_console_bug_when_input.png)



![image-20201027163651804](L:\资料\设计方案\视频教程\hmos\pic\053_udd_num_err.png)